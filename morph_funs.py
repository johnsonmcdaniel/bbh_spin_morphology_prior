# Functions for evaluating the spin morphology
# (c) 2023 Nathan K. Johnson-McDaniel <nkjm.physics@gmail.com>

import numpy as np

# Define the solar mass in seconds; this agrees with the value used in LALSuite
# <https://git.ligo.org/lscsoft/lalsuite/-/blob/master/lal/lib/std/LALConstants.h>

MTSUN_SI = 4.925490947641266978197229498498379006e-6


def cosdeltaphi_sgn(Jsq, L, Ssq, S1, S2, chieff, q):
    """
    Compute a function with the same sign as cos Delta Phi [Eq. (3) in the paper].
    Only applicable for unequal-mass cases--it gives infinity for exactly equal masses

    Inputs (all angular momenta in total mass = 1 units):
    Jsq: Square of the magnitude of the total angular momentum
    L: Magnitude of the orbital angular momentum
    Ssq: Square of the magnitude of the total spin angular momentum
    S1, S2: Magnitudes of the individual spin angular momenta
    chieff: Effective spin
    q: Mass ratio (with q < 1)

    Output:
    Function with the same sign as cos Delta Phi
    """

    # Compute useful expressions

    chieff_oo_opq = chieff/(1. + q)

    omq = 1. - q

    SdotL = (Jsq - L*L - Ssq)/(2.*L)

    return Ssq - S1*S1 - S2*S2 + 2.*q*(SdotL - q*chieff_oo_opq)*(SdotL - chieff_oo_opq)/(omq*omq)


def morph_fun_analytic(v, q, chi1, chi2, tilt1, tilt2, phi12, LPNorder=1.5):
    """
    Return the morphology as an integer (0 for L0, 2 for C, 4 for Lpi)

    This implements the calculation given in Sec. II of the paper and also computes the PN orbital angular momentum

    It is only applicable for unequal-mass cases--it gives infinity for exactly equal masses

    Inputs:
    v: Binary's (PN) orbital velocity
    q: Mass ratio (with q < 1)
    chi1, chi2: Magnitudes of dimensionless spins
    tilt1, tilt2: Spin tilts
    phi12: Angle between in-plane spins
    LPNorder: PN order to use for orbital angular momentum calculation, choices 0, 1, 1.5, 2, and 2.5 (default: 1.5)

    Output:
    Integer giving the morphology
    """

    # Compute useful quantities

    opq = 1. + q
    omq = 1. - q

    op_ooq = 1. + 1./q

    X1sq = 1./(opq*opq)

    X2sq = q*q*X1sq

    ct1 = np.cos(tilt1)
    ct2 = np.cos(tilt2)

    # Compute spin quantities

    S1 = chi1*X1sq
    S2 = chi2*X2sq

    S1z = S1*ct1
    S2z = S2*ct2

    S1dotS2_term = 2.*S1*S2*(np.sin(tilt1)*np.sin(tilt2)*np.cos(phi12) + ct1*ct2)

    Ssq = S1*S1 + S2*S2 + S1dotS2_term

    # Compute L, Jsq, and xi; the orbital angular momentum expressions come from Eq. (4) in https://dcc.ligo.org/T1500554/public, with the
    # spins orbit-averaged as in Sec. II of that reference

    eta = q*X1sq

    vsq = v*v

    Lz_scaled = 1./v

    if LPNorder >= 1:
        Lz_scaled += (1.5 + eta/6.)*v
    if LPNorder >= 1.5:
        Lz_scaled += -((1. + 27.*opq)*S1z + (1. + 27.*op_ooq)*S2z)*vsq/12.

        S1_coeff = (-0.25 - 0.75*opq)*vsq
        S2_coeff = (-0.25 - 0.75*op_ooq)*vsq
    if LPNorder >= 2:
        Lz_scaled += (3.375 - 2.375*eta + eta*eta/24.)*vsq*v
    if LPNorder >= 2.5:
        v4_term = vsq*vsq/48.

        Lz_scaled += ((1. - 147.*opq - 367./(3.*opq) + 13.*X1sq/3.)*S1z + (1. - 147.*op_ooq - 367./(3.*op_ooq) + 13.*X2sq/3.)*S2z)*v4_term

        S1_coeff += (-71. + 21.*opq - 13./opq - 9.*X1sq)*v4_term
        S2_coeff += (-71. + 21.*op_ooq - 13./op_ooq - 9.*X2sq)*v4_term

    if LPNorder >= 1.5:
        Lsq = eta*eta*(Lz_scaled*Lz_scaled + 2.*Lz_scaled*(S1_coeff*S1z + S2_coeff*S2z) + S1_coeff*S1_coeff*S1*S1 + S2_coeff*S2_coeff*S2*S2 +
                       S1_coeff*S2_coeff*S1dotS2_term)
        L = Lsq**0.5
    else:
        L = eta*Lz_scaled
        Lsq = L*L

    Jsq = Lsq + Ssq + 2.*L*(S1*ct1 + S2*ct2)

    xi = opq*(S1*ct1 + S2*ct2/q)

    # Compute more useful combinations

    S1sq = S1*S1
    S2sq = S2*S2

    xx = omq*(q*S1sq - S2sq)
    yy = omq*(S1sq - q*S2sq)
    zz = omq*(S1sq - S2sq)

    JLsq = Jsq - Lsq

    JL4 = JLsq*JLsq

    # Compute coefficients of cubic [Eqs. (1) in the paper]

    BB = -2.*Jsq + 2.*L*xi + (Lsq*(1. + q*q) - xx)/q

    CC = JL4 - 2.*JLsq*L*xi + 4.*Lsq*q*xi*xi/(opq*opq) + 2.*(Jsq*xx - Lsq*yy)/q - 2.*L*xi*zz/opq

    DD = (Lsq*zz*zz - JL4*xx)/q + 2.*JLsq*L*xi*zz/opq

    # Compute pieces of solution [see Eqs. (2) in the paper]

    BB2 = BB*BB
    BB3 = BB2*BB

    disc = (BB2 - 4.*CC)*CC*CC - (27.*DD + 4.*BB3 - 18.*BB*CC)*DD

    cr_bit_real = -2.*BB3 + 9.*BB*CC - 27.*DD
    cr_bit_imag = 3.*(3.*np.maximum(disc,0))**0.5 # Account for cases where disc is slightly negative due to floating-point errors

    Q1_arg = np.arctan2(cr_bit_imag, cr_bit_real)/3.

    Q1_mag_quant = 2.*(BB2 - 3.*CC)**0.5/3.

    BB_scaled = -BB/3.

    # Put together solutions [Eqs. (2) in the paper]

    Smin_sq = BB_scaled - Q1_mag_quant*np.cos(Q1_arg + np.pi/3.)
    Smax_sq = BB_scaled + Q1_mag_quant*np.cos(Q1_arg)

    # Compute the signs of cos Delta Phi at Smin_sq and Smax_sq

    cdphi_sgn_Smin = cosdeltaphi_sgn(Jsq, L, Smin_sq, S1, S2, xi, q)
    cdphi_sgn_Smax = cosdeltaphi_sgn(Jsq, L, Smax_sq, S1, S2, xi, q)

    # Construct the morphology

    cdphi_sgn_Smin_sign = cdphi_sgn_Smin/abs(cdphi_sgn_Smin)
    cdphi_sgn_Smax_sign = cdphi_sgn_Smax/abs(cdphi_sgn_Smax)

    return abs(2. - cdphi_sgn_Smin_sign - cdphi_sgn_Smax_sign)


def morph_int(m1, m2, chi1, chi2, tilt1, tilt2, phi12, f_ref=20., LPNorder=1.5):
    """
    Function to output an integer for the different morphologies (0 for L0, 2 for C, and 4 for Lpi)
    given the binary's individual masses, spins, and reference frequency

    It is only applicable for unequal-mass cases--it gives infinity for exactly equal masses

    Input:
    m1, m2: Individual masses in solar masses
    chi1, chi2: Magnitudes of dimensionless spins
    tilt1, tilt2: Spin tilts
    phi12: Angle between in-plane spins
    f_ref: Reference frequency in Hz (default: 20 Hz)
    LPNorder: PN order to use for orbital angular momentum calculation, choices 0, 1, 1.5, 2, and 2.5 (default: 1.5)

    Output:
    Morphology integer
    """

    # Make single entries into arrays
    m1 = np.atleast_1d(m1)
    m2 = np.atleast_1d(m2)

    chi1 = np.atleast_1d(chi1)
    chi2 = np.atleast_1d(chi2)

    tilt1 = np.atleast_1d(tilt1)
    tilt2 = np.atleast_1d(tilt2)
    phi12 = np.atleast_1d(phi12)
    
    # Compute binary's (PN) orbital velocity
    v = (np.pi*(m1 + m2)*MTSUN_SI*f_ref)**(1./3.)

    # Swap bodies if m1 < m2
    idx_swap = np.where(m1 < m2)

    if len(idx_swap[0]) > 0:
        m1[idx_swap], m2[idx_swap] = m2[idx_swap], m1[idx_swap]
        chi1[idx_swap], chi2[idx_swap] = chi2[idx_swap], chi1[idx_swap]
        tilt1[idx_swap], tilt2[idx_swap] = tilt2[idx_swap], tilt1[idx_swap]
        phi12[idx_swap] = 2.*np.pi-phi12[idx_swap]

    q = m2/m1

    return morph_fun_analytic(v, q, chi1, chi2, tilt1, tilt2, phi12, LPNorder=LPNorder)
