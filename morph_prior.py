# Defining a class to allow sampling with the prior restricted to a given precessional morphology in Bilby
# One has to set the reference frequency by hand here; by default it is 20 Hz
# Similarly, the default setting for the PN order for the orbital angular momentum used in the morphology calculation is 1.5PN
# Based on the BBHPriorDict class from gw/prior.py in Bilby
# (c) 2023 Nathan K. Johnson-McDaniel <nkjm.physics@gmail.com>

import os

import bilby.gw.prior

from bilby.core.utils import logger

from bilby.core.prior import Constraint, DeltaFunction

from bilby.gw.prior import CBCPriorDict

from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters, fill_from_fixed_priors, generate_mass_parameters

from morph_funs import morph_int

# Set reference frequency and PN order of orbital angular momentum calculation used in morphology calculation

f_ref = 20.  # Hz

LPNorder = 1.5  # Possible settings are 0, 1, 1.5, 2, and 2.5

# Set Bilby default prior directory

DEFAULT_PRIOR_DIR = os.path.join(os.path.dirname(bilby.gw.prior.__file__), 'prior_files')


class BBHPriorDictMorphology(CBCPriorDict):
    def __init__(self, dictionary=None, filename=None,
                 conversion_function=None):
        """ Initialises a Prior set for Binary Black holes allowing one to
        restrict to a given precessional morphology.

        Parameters
        ==========
        dictionary: dict, optional
            See superclass
        filename: str, optional
            See superclass
        conversion_function: func
            Function to convert between sampled parameters and constraints.
            By default this generates many additional parameters, see
            BBHPriorDictMorphology.default_conversion_function
        """
        if dictionary is None and filename is None:
            fname = 'precessing_spins_bbh.prior'

            filename = os.path.join(DEFAULT_PRIOR_DIR, fname)
            logger.info('No prior given, using default BBH priors in {}.'.format(filename))
        elif filename is not None:
            if not os.path.isfile(filename):
                filename = os.path.join(DEFAULT_PRIOR_DIR, filename)
        super(BBHPriorDictMorphology, self).__init__(dictionary=dictionary, filename=filename,
                                                     conversion_function=conversion_function)

    def default_conversion_function(self, sample):
        """
        Default parameter conversion function for BBH signals, augmented with the morphology computation.

        This generates:
        - the parameters passed to source.lal_binary_black_hole
        - all mass parameters
        - the morphology integer

        It does not generate:
        - component spins
        - source-frame parameters

        Parameters
        ==========
        sample: dict
            Dictionary to convert

        Returns
        =======
        sample: dict
            Same as input
        """
        out_sample = fill_from_fixed_priors(sample, self)
        out_sample, _ = convert_to_lal_binary_black_hole_parameters(out_sample)
        out_sample = generate_mass_parameters(out_sample)

        out_sample['morph_int'] = morph_int(out_sample['mass_1'], out_sample['mass_2'], out_sample['a_1'],
                                            out_sample['a_2'], out_sample['tilt_1'], out_sample['tilt_2'],
                                            out_sample['phi_12'], f_ref=f_ref, LPNorder=LPNorder)

        return out_sample

    def test_redundancy(self, key, disable_logging=False):
        """
        Test whether adding the key would add be redundant.
        Already existing keys return True.

        Parameters
        ==========
        key: str
            The key to test.
        disable_logging: bool, optional
            Disable logging in this function call. Default is False.

        Returns
        ======
        redundant: bool
            Whether the key is redundant or not
        """
        if key in self:
            logger.debug('{} already in prior'.format(key))
            return True

        sampling_parameters = {key for key in self if not isinstance(
            self[key], (DeltaFunction, Constraint))}

        mass_parameters = {'mass_1', 'mass_2', 'chirp_mass', 'total_mass', 'mass_ratio', 'symmetric_mass_ratio'}
        spin_tilt_1_parameters = {'tilt_1', 'cos_tilt_1'}
        spin_tilt_2_parameters = {'tilt_2', 'cos_tilt_2'}
        spin_azimuth_parameters = {'phi_1', 'phi_2', 'phi_12', 'phi_jl'}
        inclination_parameters = {'theta_jn', 'cos_theta_jn'}
        distance_parameters = {'luminosity_distance', 'comoving_distance', 'redshift'}
        morphology_parameters = {'morph_int'}

        for independent_parameters, parameter_set in \
                zip([2, 2, 1, 1, 1, 1, 1],
                    [mass_parameters, spin_azimuth_parameters,
                     spin_tilt_1_parameters, spin_tilt_2_parameters,
                     inclination_parameters, distance_parameters,
                     morphology_parameters]):
            if key in parameter_set:
                if len(parameter_set.intersection(
                        sampling_parameters)) >= independent_parameters:
                    logger.disabled = disable_logging
                    logger.warning('{} already in prior. '
                                   'This may lead to unexpected behaviour.'
                                   .format(parameter_set.intersection(self)))
                    logger.disabled = False
                    return True
        return False
