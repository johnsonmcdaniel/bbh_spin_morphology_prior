# Setup script to install the code for restricting the precessional morphology in Bilby runs
# (c) 2023 Nathan K. Johnson-McDaniel <nkjm.physics@gmail.com>

from setuptools import setup

setup(
    name='bbh_spin_morphology_prior',
    version=1.3,
    install_requires=['bilby'],
    py_modules=['morph_funs', 'morph_prior'],
)
