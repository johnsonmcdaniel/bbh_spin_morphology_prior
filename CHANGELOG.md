## [1.3] 2023-07-24

### Changes
- In `morph_fun_analytic()` in `morph_funs.py`, take the maximum of `disc` and 0 in the square root to prevent NANs in cases where disc is slightly negative due to floating-point errors.

## [1.2] 2023-04-15

### Changes
- Added `np.atleast_1d()` to parameters of `morph_int()` to prevent errors when a single value is passed

## [1.1] 2023-02-20

### Changes
- Updated `MTSUN_SI` in `morph_funs.py` to match the updated version in LALSuite that uses the nominal IAU value (fractional change of ~2e-8)
