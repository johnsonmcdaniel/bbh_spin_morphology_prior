# bbh_spin_morphology_prior

A package of functions to accompany
[Bilby](https://git.ligo.org/lscsoft/bilby) for the analysis of gravitational
wave signals from binary black holes. Specifically, these functions allow one to restrict the
prior to a given precessional morphology, as described in

[arXiv:2301.10125](https://arxiv.org/abs/2301.10125)

Any work that uses this code should cite that paper.

Note that this code does not work for exactly equal-mass cases. However, this will not be a problem in stochastic sampling
unless one fixes the mass ratio to exactly 1.

## Installation

Install this code in the same environment as Bilby/[parallel
Bilby](https://git.ligo.org/lscsoft/parallel_bilby) using
```
python setup.py install
```

This has been tested with Bilby 1.1.5, but should also work with other versions.

## Settings

The default settings are a reference frequency of 20 Hz and the 1.5PN order expression for the orbital angular momentum,
as used in the paper referenced above.

To change these, edit the f_ref and LPNorder settings in [morph_prior.py](https://gitlab.com/johnsonmcdaniel/bbh_spin_morphology_prior/-/blob/master/morph_prior.py).

The possible LPNorder values are 0, 1, 1.5, 2, and 2.5.

## Usage

When using [bilby_pipe](https://git.ligo.org/lscsoft/bilby_pipe) or parallel Bilby, set
```python
default-prior=morph_prior.BBHPriorDictMorphology
```
in the `ini` file and then add
```python
morph_int = Constraint(<low>, <high>)
```
to the prior file, where `<low>` and `<high>` surround the desired morphology integer (0, 2, or 4, for L0, C, or Lpi)
and no other, e.g., -0.5, 0.5; 1.5, 2.5; 3.5, 4.5 for L0; C; Lpi. This requires the bilby_pipe version to be at least 1.0.7.

When using Bilby by itself, just use
```python
import morph_prior

from bilby.core.prior import Constraint

priors = morph_prior.BBHPriorDictMorphology()
priors['morph_int'] = Constraint(<low>, <high>)
```
with the same `<low>` and `<high>` values discussed above, and set up the priors on the other parameters as usual.

## License

This code is released under the MIT license in [LICENSE.md](https://gitlab.com/johnsonmcdaniel/bbh_spin_morphology_prior/-/blob/master/LICENSE.md)
